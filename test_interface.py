import os
import matplotlib.pyplot as plt
import numpy as np
from prettytable import PrettyTable




parteien=[] #Liste der Parteien
voter=[] #Liste der Ergebnisse
color=[] #Liste der Farben
zaehler=0 #Wird fuer spaeter gebraucht

def clear(): os.system('clear')

#Abfrage, ob alle Eingaben passen. Erinnerung an mich: Wenn nicht passen, dann nochmal Aenderungen moeglich machen.
def abfrageok():
    print("Passen die Eingaben?")
    print(tab)
    abfr=input()

    if abfr.lower() == "nein":
        exit()
    elif abfr.lower() == "ja":
        pass
    else:
        print("Eingabe nicht verstanden. Bitte ja / nein eingeben")
        abfrageok()


def genBalken():
#Angabe der Balken
    print("Wie viele Balken sollen dargestellt werden?")
    anzBalk=int(input())
    clear()
    global zaehler

    while zaehler < anzBalk:
        zaehler +=1 

        print(f"Name der {zaehler}. Partei:")
        beschr=input()

        print(f"Ergebnis der {beschr}:")
        ergeb=float(input())

        print(f"Farbe der {beschr}:")
        farb=input()

        parteien.append(beschr)
        voter.append(ergeb)
        color.append(farb)

        clear()
    zaehler = 0

    while zaehler < anzBalk:
        tab.add_row([parteien[zaehler], str(voter[zaehler]), str(color[zaehler])])
        zaehler += 1






#Erstellen einer Tabelle
tab = PrettyTable()
tab.field_names = ["Partei", "Ergebnis", "Farbe"]


clear()
genBalken() #Zeit, die Balken zu generieren

abfrageok() #Pruefung, ob alles passt



#Falls ich mal ein # vergesse
loopcount = 0
for i in color:
    if not i.startswith("#"):
        color[loopcount] = "#"+i
    loopcount += 1





fig, ax = plt.subplots()

#Festlegung der Partieien mit Ergebnis und Farbe
#parteien = ('Grün-Rot', 'TPD', 'KPD', 'SMRP', 'CDU', 'SAPD', 'AfD')
#voter = [44.65,25.22,10.71,6.08,6.02,4.38,2.91]
y_pos = np.arange(len(parteien))
#color = ['#e3000f', '#00ffff', '#8b0000', '#ae00ff', '#000000', '#a40051', '#009ee0']

plt.title('Ergebnis der Zweitstimmen')
plt.ylim(0,50)



#Darstellung der Prozentpunkte oberhalb der Balken
c_rects = plt.bar(y_pos, voter, align='center', color=color)
def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., height + 1,
            '%.1f' % float(height),
            ha='center', va='bottom')
autolabel(c_rects)


#5-Prozenthürde
plt.axhline(y=5,ls='--',linewidth=0.5,color='#000000')



#Formatierung x-Achse
ax.set_xticks(range(len(parteien)))
ax.set_xticklabels(parteien, rotation='horizontal')


#Anpassung der Bilddatei an die Größe der Figur
plt.tight_layout()


#Ausblenden der oberen und linken Linie
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)

#Setzen der Hintergrundfarbe innerhalb der Figur. Die Farbe außerhalb wird im plt.savefig durch den Parameter "facecolor" gesetzt
ax.set_facecolor('#e7eef4')

print("Dateiname:")
dname=input()

plt.savefig(f'{dname}.png', dpi=800, facecolor='#e7eef4')

plt.show()
