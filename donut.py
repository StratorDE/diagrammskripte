import matplotlib.pyplot as plt
import numpy as np


fig, ax = plt.subplots()

#Festlegung der Partieien mit Ergebnis und Farbe
parteien = ('CDU', 'TPD', 'Grün-Rot', 'SAPD', 'SMRP', 'KPD', '')
voter = [1,4,8,1,1,2,17]
y_pos = np.arange(len(parteien))
color = ['#000000', '#00ffff', '#e3000f', '#a40051', '#ae00ff', '#8b0000', 'white']

plt.title('Sitzverteilung')
plt.ylim(0,50)

def absolute_value(val):
    a = voter[1]
    return a


plt.rc('hatch', color='#009900', linewidth=8.0)
patches = plt.pie(voter,colors=color,labels=parteien, autopct=absolute_value)[0]
patches[2].set_hatch('/')


m_circle=plt.Circle( (0,0), 0.4, color='white')
p=plt.gcf()
p.gca().add_artist(m_circle)


plt.show()
