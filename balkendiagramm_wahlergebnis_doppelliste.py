import matplotlib.pyplot as plt
import numpy as np


fig, ax = plt.subplots()

#Festlegung der Partieien mit Ergebnis und Farbe
parteien = ('Grün-Rot', 'TPD', 'KPD', 'SMRP', 'CDU', 'SAPD', 'AfD')
voter = [44.65,25.22,10.71,6.08,6.02,4.38,2.91]
y_pos = np.arange(len(parteien))
color = ['#e3000f', '#00ffff', '#8b0000', '#ae00ff', '#000000', '#a40051', '#009ee0']

plt.title('Ergebnis der Zweitstimmen')
plt.ylim(0,50)

#Nur wichtig, falls es sich bei einer Liste um eine gemeinsame Liste einer Partei handelt. Legt die Größe sowie Farbe des Hatches fest. Muss beim Balken mit "hatch='/'" definiert werden.
plt.rc('hatch', color='#009900', linewidth=8.0)

#Darstellung der Balken
ax.bar(0, voter[0], align='center',color=color[0], hatch='/') #<- Hatch macht zweifarbige Darstellung der Liste möglich
ax.bar(1, voter[1], align='center',color=color[1])
ax.bar(2, voter[2], align='center',color=color[2])
ax.bar(3, voter[3], align='center',color=color[3])
ax.bar(4, voter[4], align='center',color=color[4])
ax.bar(5, voter[5], align='center',color=color[5])
ax.bar(6, voter[6], align='center',color=color[6])



#Darstellung der Prozentpunkte oberhalb der Balken
c_rects = plt.bar(y_pos, voter, align='center', alpha=0)
def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., height + 1,
            '%.1f' % float(height),
            ha='center', va='bottom')
autolabel(c_rects)


#5-Prozenthürde
plt.axhline(y=5,ls='--',linewidth=0.5,color='#000000')



#Formatierung x-Achse
ax.set_xticks(range(len(parteien)))
ax.set_xticklabels(parteien, rotation='horizontal')


#Anpassung der Bilddatei an die Größe der Figur
plt.tight_layout()


#Ausblenden der oberen und linken Linie
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)

#Setzen der Hintergrundfarbe innerhalb der Figur. Die Farbe außerhalb wird im plt.savefig durch den Parameter "facecolor" gesetzt
ax.set_facecolor('#e7eef4')


plt.savefig('test3.png', dpi=800, facecolor='#e7eef4')

plt.show()
