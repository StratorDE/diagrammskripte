import matplotlib.pyplot as plt
import numpy as np


fig, ax = plt.subplots()

#Festlegung der Partieien mit Ergebnis und Farbe
parteien = ('Grün-Rot', 'TPD', 'KPD', 'SMRP', 'CDU', 'SAPD')
voter = [8,4,2,1,1,1]
y_pos = np.arange(len(parteien))
color = ['#e3000f', '#00ffff', '#8b0000', '#ae00ff', '#000000', '#a40051']

plt.title('Sitzverteilung')
plt.ylim(0,50)

plt.rc('hatch', color='#009900', linewidth=8.0)
patches = plt.pie(voter,colors=color,labels=parteien, autopct='%1.1f%%')[0]
patches[0].set_hatch('/')


plt.show()
