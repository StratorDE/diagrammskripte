import matplotlib.pyplot as plt
import numpy as np


fig, ax = plt.subplots()

#Festlegung der Partieien mit Ergebnis und Farbe
parteien = ('PPD', 'TPD', 'FDP', 'Linkere', 'AEßX20', 'CDU')
voter = [35.00,16.4,14.7,14.5,12.6,6.8]
y_pos = np.arange(len(parteien))
color = ['#e3000f', '#00ffff', '#ffed00', '#a40051', '#ae00ff', '#000000']



#parteien = ('PPD', 'TPD', 'Linkere', 'FDP', 'AEßX20', 'CDU')
#color = ['#e3000f', '#00ffff', '#a40051', '#ffed00', '#ae00ff', '#000000']

plt.title('Ergebnis der Zweitstimmen (mit simulierten Stimmen)')
plt.ylim(0,50)



#Darstellung der Prozentpunkte oberhalb der Balken
c_rects = plt.bar(y_pos, voter, align='center', color=color)
def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., height + 1,
            '%.1f' % float(height),
            ha='center', va='bottom')
autolabel(c_rects)


#5-Prozenthürde
plt.axhline(y=5,ls='--',linewidth=0.5,color='#000000')



#Formatierung x-Achse
ax.set_xticks(range(len(parteien)))
ax.set_xticklabels(parteien, rotation='horizontal')


#Anpassung der Bilddatei an die Größe der Figur
plt.tight_layout()


#Ausblenden der oberen und linken Linie
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)

#Setzen der Hintergrundfarbe innerhalb der Figur. Die Farbe außerhalb wird im plt.savefig durch den Parameter "facecolor" gesetzt
ax.set_facecolor('#e7eef4')


plt.savefig('test2.png', dpi=800, facecolor='#e7eef4')

plt.show()
